#pragma once

#include "Vector4.h"
#include "Vector3.h"

class Light {
public:
	Light() { position = Vector3(0, 0, 0); colour = Vector4(1, 1, 1, 1); radius = 1.0f; shadows = false; };

	Light(Vector3 position, Vector4 colour, float radius) {
		this->position = position;
		this->colour = colour;
		this->radius = radius;
	}

	~Light(void) {}

	Vector3 GetPosition() const { return position; }
	void SetPosition(Vector3 val) { position = val; }

	Vector4 GetColour() const { return colour; }
	void SetColour(Vector4 val) { colour = val; }

	float GetRadius() const { return radius; }
	void SetRadius(float val) { radius = val; }

	bool castShadows() const { return shadows; }
	void setCastShadows(bool val) { shadows = val; }

	Vector3 getShadowDir() const { return shadowDir; }
	void setShadowDir(Vector3 dir) { shadowDir = dir; }

protected:
	Vector3 shadowDir;
	Vector3 position;
	Vector4 colour;
	float radius;
	bool shadows;
};

