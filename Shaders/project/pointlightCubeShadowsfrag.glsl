#version 150 core

uniform sampler2D depthTex;
uniform sampler2D normTex;
uniform sampler2D posTex;
uniform samplerCubeShadow shadowTex;
//uniform samplerCube shadowTex;
uniform mat4 shadowMVP;
uniform float far_plane;
uniform float near_plane;

uniform mat4 viewMatrix;
uniform mat4 modelMatrix;

uniform vec2 pixelSize;
uniform vec3 cameraPos;

uniform float lightRadius;
uniform vec3 lightPos;
uniform vec4 lightColour;
uniform bool castShadows;

in mat4 inverseProjView;
out vec4 fragColour[2];

/*float ShadowCalculation(vec3 fragPos) {
	vec3 fragToLight = fragPos - lightPos;
	float closestDepth = texture(shadowTex, fragToLight).r;
	//float foo = 2.0 * closestDepth - 1.0;
	//float bar = 2.0 * near_plane * far_plane / (far_plane + near_plane - foo * (far_plane - near_plane));
	//closestDepth *= far_plane;
	float poo = (2 * 1) / (10000 + 1 - closestDepth * (10000 - 1));
	float currentDepth = length(fragToLight);
	float bias = 0.05;
	float shadow = currentDepth - bias > poo ? 1.0 : 0.0;
	
	return shadow;
}*/

void main(void) {
	vec3 pos = vec3((gl_FragCoord.x * pixelSize.x), (gl_FragCoord.y * pixelSize.y), 0.0);
	pos.z = texture(depthTex, pos.xy).r;
	vec3 posLinDepth = pos;
	posLinDepth.z = (2 * 1) / (10000 + 1 - posLinDepth.z * (10000 - 1));
	
	vec3 normal = normalize(texture(normTex, pos.xy).xyz * 2.0 - 1.0);
	
	vec3 norm = texture(normTex, pos.xy).xyz;
	
	vec4 clip = inverseProjView * vec4(pos * 2.0 - 1.0, 1.0);
	pos = clip.xyz / clip.w;
	
	float dist = length(lightPos - pos);
	float atten = 1.0 - clamp(dist / lightRadius, 0.0, 1.0);
	
	if(atten == 0.0) {
		discard;
	}
	
	vec3 incident = normalize(lightPos - pos);
	vec3 viewDir = normalize(cameraPos - pos);
	vec3 halfDir = normalize(incident + viewDir);
	
	float lambert = clamp(dot(incident, normal), 0.0, 1.0);
	float rFactor = clamp(dot(halfDir, normal), 0.0, 1.0);
	float sFactor = pow(rFactor, 33.0);
	
	float shadow = 1.0;
	if(castShadows) {
		//shadow = ShadowCalculation(pos);
		//vec3 fragPos = vec3((gl_FragCoord.x * pixelSize.x), (gl_FragCoord.y * pixelSize.y), 0.0);
		//shadow = ShadowCalculation(pos);
		
		vec4 shadowProj = (shadowMVP * vec4(pos + (normal), 1));
		//float shadow = texture(shadowTex, vec4(pos, 1.0));
		//vec3 posToLight = pos - lightPos;
		//float shadow = texture(shadowTex, posToLight).r;
		//shadow = (2 * 1) / (10000 + 1 - shadow * (10000 - 1));
		vec4 trueLightPos = modelMatrix * vec4(0,0,0, 1.0);
		vec3 foo = pos - trueLightPos.xyz;
		float shadow = texture(shadowTex, vec4(foo, 1.0));
		//float shadow = ShadowCalculation(pos);
		
		//Linear depth func
		//(2 * 1) / (10000 + 1 - shadow * (10000 - 1));
	
		lambert *= shadow;
		sFactor *= shadow;
	}
	
	fragColour[0] = vec4(lightColour.xyz * lambert * atten, 1.0);
	fragColour[1] = vec4(lightColour.xyz * sFactor * atten * 0.33, 1.0);
	
	//fragColour[0] = vec4(shadow, shadow, shadow, 1.0);
	//fragColour[0] = vec4(pos, 1.0);
	//fragColour[0] = vec4(lightPos, 1.0);
}