#version 150

uniform sampler2DShadow shadowTex;

in Vertex {
	vec4 shadowProj;
} IN;

out vec4 fragColour;

void main(void) {
	float shadow = 1.0;
	if(IN.shadowProj.w > 0.0) {
		shadow = textureProj(shadowTex, IN.shadowProj);
	}
	fragColour = vec4(shadow,shadow,shadow, 1.0);
}