// Project.cpp : This file contains the 'main' function. Program execution begins and ends there.
//
#include "pch.h"

#pragma comment(lib, "nclgl.lib")

#include "Renderer.h"
#include "../../nclgl/Window.h"

int main() {
	//Window w("Project", 1920, 1200, true);
	//Window w("Project", 2560, 1440, true);
	//Window w("Project", 1920, 1200, false);
	//Window w("Project", 1080, 720, false);
	Window w("Project", 1280, 720, false);

	if (!w.HasInitialised()) {
		return -1;
	}

	Renderer renderer(w);

	if (!renderer.HasInitialised()) {
		return -1;
	}

	w.LockMouseToWindow(true);
	w.ShowOSPointer(false);

	while (w.UpdateWindow() && !Window::GetKeyboard()->KeyDown(KEYBOARD_ESCAPE)) {
		renderer.UpdateScene(w.GetTimer()->GetTimedMS());
		renderer.RenderScene();
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_P)) {
			renderer.toggleDrawHUD();
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_I)) {
			renderer.toggleRedPostProcess();
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_U)) {
			renderer.toggleDrawSplitScreen();
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_PAUSE)) {
			renderer.toggleCycleScenes();
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_RIGHT)) {
			renderer.switchScene();
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_LEFT)) {
			renderer.switchScene(false);
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_J)) {
			renderer.toggleMoveCamOne();
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_O)) {
			renderer.toggleUseCubeShadows();
		}
		if (Window::GetKeyboard()->KeyTriggered(KEYBOARD_L)) {
			renderer.toggleCastShadows();
		}
	}

	return 0;
}