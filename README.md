# Advanced Graphics For Games Project
## Project Overview
### Objective
The objective of this coursework was to showcase advanced graphical techniques in OpenGL

The program makes use of features such as Deferred Lighting, Shadow Maps, Cube Maps, Skeletal Animation, Scene Graphs, Index Buffers, Height Maps, Bump Maps

### Video Demo
https://www.youtube.com/watch?v=7-mGZMdOju4

### Controls
* WSAD,Shift,Space - Move the camera
* Left,Right - Cycle through the scenes
* P - Toggle the HUD
* U - Toggle Splitscreen
* J - Toggle which screen camera to move
* I - Toggle post processing
* Pause - Toggle auto scene cycle
* L - Toggle shadows
* O - Toggle cube mapped shadows

## Build Information
The solution file is in the **Graphics For Games** folder

It is currently only setup to build for x64

Build directories are in the **Graphics For Games** folder then either **Release** or **Debug**
