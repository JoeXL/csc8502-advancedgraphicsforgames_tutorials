#include "pch.h"
#include "Renderer.h"

Renderer::Renderer(Window& parent) : OGLRenderer(parent) {
	camOne = new Camera(-10.0f, 180.0f, Vector3(16.0f, 350.0f, -805.0f));
	camOne->SetSpeed(0.1f);

	camTwo = new Camera(-10.0f, 180.0f, Vector3(16.0f, 350.0f, -805.0f));
	camTwo->SetSpeed(0.1f);

	cameraPerspectiveMatrix = Matrix4::Perspective(1.0f, 10000.0f, (float)width / (float)height, 45.0f);
	cameraSplitScreenPerspectiveMatrix = Matrix4::Perspective(1.0f, 10000.0f, (float)width / ((float)height / 2.0f), 45.0f);
	shadowPerspectiveMatrix = Matrix4::Perspective(1.0f, 10000.0f, 1, 90.0f);

	sceneOne = new SceneNode;
	sceneTwo = new SceneNode;
	sceneThree = new SceneNode;

	fullScreenQuad = Mesh::GenerateQuad();
	root = sceneOne;
	lights = &lightsSceneOne;

	fps = 0;
	frameCount = 0;
	fpsCounter = 0;

	sceneShader = new Shader(SHADERDIR"/project/BumpVertex.glsl", SHADERDIR"/project/bufferFragment.glsl");
	combineShader = new Shader(SHADERDIR"/project/combinevert.glsl", SHADERDIR"/project/combinefrag.glsl");
	pointLightShader = new Shader(SHADERDIR"/project/pointlightvert.glsl", SHADERDIR"/project/pointlightfrag.glsl");
	pointLightShaderCubeShadows = new Shader(SHADERDIR"/project/pointlightCubeShadowsvert.glsl", SHADERDIR"/project/pointlightCubeShadowsfrag.glsl");
	shadowShader = new Shader(SHADERDIR"/project/shadowVert.glsl", SHADERDIR"/project/shadowFrag.glsl");
	shadowCubeShader = new Shader(SHADERDIR"/project/shadowCubeVert.glsl", SHADERDIR"/project/shadowCubeFrag.glsl");
	textShader = new Shader(SHADERDIR"/project/textVertex.glsl", SHADERDIR"/project/textFragment.glsl");
	reflectShader = new Shader(SHADERDIR"/project/BumpVertex.glsl", SHADERDIR"/project/reflectFragment.glsl");
	presentShader = new Shader(SHADERDIR"/project/presentVert.glsl", SHADERDIR"/project/presentFrag.glsl");
	redShader = new Shader(SHADERDIR"/project/presentVert.glsl", SHADERDIR"/project/redFrag.glsl");

	if (!sceneShader->LinkProgram() || !combineShader->LinkProgram() || !pointLightShader->LinkProgram() || !shadowShader->LinkProgram() || !shadowCubeShader->LinkProgram() || !textShader->LinkProgram() || !reflectShader->LinkProgram() || !presentShader->LinkProgram() || !pointLightShaderCubeShadows->LinkProgram() || !redShader->LinkProgram()) {
		return;
	}

	basicFont = new Font(SOIL_load_OGL_texture(TEXTUREDIR"tahoma.tga", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_COMPRESS_TO_DXT), 16, 16);

	setupSceneOne();
	setupSceneTwo();
	setupSceneThree();

	lightSphere = new OBJMesh();
	if (!lightSphere->LoadOBJMesh(MESHDIR"ico.obj")) {
		return;
	}

	skybox = SOIL_load_OGL_cubemap(
		TEXTUREDIR"rusted_west.jpg", TEXTUREDIR"rusted_east.jpg",
		TEXTUREDIR"rusted_up.jpg", TEXTUREDIR"rusted_down.jpg",
		TEXTUREDIR"rusted_south.jpg", TEXTUREDIR"rusted_north.jpg",
		SOIL_LOAD_RGB, SOIL_CREATE_NEW_ID, 0
	);

	glGenFramebuffers(1, &bufferFBO);
	glGenFramebuffers(1, &pointLightFBO);
	glGenFramebuffers(1, &shadowFBO);
	glGenFramebuffers(1, &shadowCubeMapFBO);
	glGenFramebuffers(1, &combinedFBO);

	gBuffer[0] = GL_COLOR_ATTACHMENT0;
	gBuffer[1] = GL_COLOR_ATTACHMENT1;
	gBuffer[2] = GL_COLOR_ATTACHMENT2;

	lightBuffer[0] = GL_COLOR_ATTACHMENT0;
	lightBuffer[1] = GL_COLOR_ATTACHMENT1;

	combinedBuffer[0] = GL_COLOR_ATTACHMENT0;

	generateGBuffer();
	generateLightBuffer();
	generateShadowBuffer();
	generateShadowCubeMapBuffer();
	generateCombinedBuffer();

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS);

	init = true;
}


Renderer::~Renderer() {
	for (int i = 0; i < lightsSceneOne.size(); ++i) {
		delete lightsSceneOne.at(i);
	}

	for (int i = 0; i < lightsSceneTwo.size(); ++i) {
		delete lightsSceneTwo.at(i);
	}

	for (int i = 0; i < lightsSceneThree.size(); ++i) {
		delete lightsSceneThree.at(i);
	}
	delete lightSphere;
	delete fullScreenQuad;
	delete camOne;
	delete camTwo;

	delete hellData;
	delete basicFont;

	delete sceneShader;
	delete pointLightShader;
	delete pointLightShaderCubeShadows;
	delete combineShader;
	delete shadowShader;
	delete shadowCubeShader;
	delete textShader;
	delete reflectShader;
	delete presentShader;
	delete redShader;

	lights = NULL;
	currentShader = NULL;

	glDeleteTextures(1, &bufferColourTex);
	glDeleteTextures(1, &bufferNormalTex);
	glDeleteTextures(1, &bufferPosTex);
	glDeleteTextures(1, &bufferDepthTex);
	glDeleteTextures(1, &lightEmissiveTex);
	glDeleteTextures(1, &lightSpecularTex);
	glDeleteTextures(1, &shadowTex);
	glDeleteTextures(1, &shadowCubeMapTex);
	glDeleteTextures(1, &combinedTex);

	glDeleteFramebuffers(1, &bufferFBO);
	glDeleteFramebuffers(1, &pointLightFBO);
	glDeleteFramebuffers(1, &shadowFBO);
	glDeleteFramebuffers(1, &shadowCubeMapFBO);
	glDeleteFramebuffers(1, &combinedFBO);

	root = NULL;

	delete sceneOne;
	delete sceneTwo;
	delete sceneThree;
}

void Renderer::GenerateScreenTexture(GLuint& into, bool depth) {
	glGenTextures(1, &into);
	glBindTexture(GL_TEXTURE_2D, into);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	glTexImage2D(GL_TEXTURE_2D, 0,
		depth ? GL_DEPTH_COMPONENT24 : GL_RGBA8,
		width, height, 0,
		depth ? GL_DEPTH_COMPONENT : GL_RGBA,
		GL_UNSIGNED_BYTE, NULL);

	glBindTexture(GL_TEXTURE_2D, 0);
}

void Renderer::GenerateShadowTexture(GLuint& into) {
	glGenTextures(1, &into);
	glBindTexture(GL_TEXTURE_2D, into);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT, SHADOWSIZE, SHADOWSIZE, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

	glBindTexture(GL_TEXTURE_2D, 0);
}

void Renderer::GenerateShadowCubeMapTexture(GLuint& into) {
	glGenTextures(1, &into);
	glBindTexture(GL_TEXTURE_CUBE_MAP, into);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

	for (int i = 0; i < 6; ++i) {
		glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, GL_DEPTH_COMPONENT24, SHADOWSIZE, SHADOWSIZE, 0, GL_DEPTH_COMPONENT, GL_UNSIGNED_BYTE, NULL);
	}

	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_COMPARE_MODE, GL_COMPARE_R_TO_TEXTURE);

	glBindTexture(GL_TEXTURE_2D, 0);
}

void Renderer::generateGBuffer() {
	GenerateScreenTexture(bufferColourTex);
	GenerateScreenTexture(bufferNormalTex);
	GenerateScreenTexture(bufferPosTex);
	GenerateScreenTexture(bufferDepthTex, true);

	glBindFramebuffer(GL_FRAMEBUFFER, bufferFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, bufferColourTex, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, bufferNormalTex, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, bufferPosTex, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, bufferDepthTex, 0);
	glDrawBuffers(3, gBuffer);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		return;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::generateLightBuffer() {
	GenerateScreenTexture(lightEmissiveTex);
	GenerateScreenTexture(lightSpecularTex);

	glBindFramebuffer(GL_FRAMEBUFFER, pointLightFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, lightEmissiveTex, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, lightSpecularTex, 0);
	glDrawBuffers(2, lightBuffer);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		return;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::generateShadowBuffer() {
	GenerateShadowTexture(shadowTex);

	glBindFramebuffer(GL_FRAMEBUFFER, shadowFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, shadowTex, 0);

	glDrawBuffer(GL_NONE);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		return;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::generateShadowCubeMapBuffer() {
	GenerateShadowCubeMapTexture(shadowCubeMapTex);

	glBindFramebuffer(GL_FRAMEBUFFER, shadowCubeMapFBO);

	glDrawBuffer(GL_NONE);

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::generateCombinedBuffer() {
	GenerateScreenTexture(combinedTex);

	glBindFramebuffer(GL_FRAMEBUFFER, combinedFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, combinedTex, 0);

	glDrawBuffers(1, combinedBuffer);

	if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
		return;
	}

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::setupSceneOne() {
	HeightMap* terrain = new HeightMap(TEXTUREDIR"smile.raw");
	terrain->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"Barren Reds.jpg", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	terrain->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"Barren RedsDOT3.JPG", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));

	SetTextureRepeating(terrain->GetTexture(), true);
	SetTextureRepeating(terrain->GetBumpMap(), true);

	SceneNode* terrainNode = new SceneNode(terrain);

	terrainNode->SetTransform(Matrix4::Translation(Vector3(-1000, -20, -1000)));
	terrainNode->SetShader(sceneShader);
	terrainNode->SetBoundingRadius(5000.0f);

	sceneOne->AddChild(terrainNode);

	OBJMesh* sphere = new OBJMesh(MESHDIR"sphere2.obj");
	SceneNode* sphereNode = new SceneNode(sphere);
	sphereNode->SetTransform(Matrix4::Scale(Vector3(5, 5, 5)) * Matrix4::Translation(Vector3(-75, 50, 0)));
	sphereNode->SetShader(reflectShader);
	sphereNode->SetBoundingRadius(500.0f);
	sceneOne->AddChild(sphereNode);

	OBJMesh* sphere2 = new OBJMesh(MESHDIR"sphere2.obj");
	sphere2->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"brick.tga", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	SetTextureRepeating(sphere2->GetTexture(), true);
	SceneNode* sphereNode2 = new SceneNode(sphere2);
	sphereNode2->SetTransform(Matrix4::Scale(Vector3(5, 5, 5)) * Matrix4::Translation(Vector3(-25, 50, 0)));
	sphereNode2->SetShader(reflectShader);
	sphereNode2->SetBoundingRadius(500.0f);
	sceneOne->AddChild(sphereNode2);

	OBJMesh* sphere3 = new OBJMesh(MESHDIR"sphere2.obj");
	sphere3->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"brickDOT3.tga", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	SetTextureRepeating(sphere3->GetBumpMap(), true);
	SceneNode* sphereNode3 = new SceneNode(sphere3);
	sphereNode3->SetTransform(Matrix4::Scale(Vector3(5, 5, 5)) * Matrix4::Translation(Vector3(25, 50, 0)));
	sphereNode3->SetShader(reflectShader);
	sphereNode3->SetBoundingRadius(500.0f);
	sceneOne->AddChild(sphereNode3);

	OBJMesh* sphere4 = new OBJMesh(MESHDIR"sphere2.obj");
	sphere4->SetTexture(sphere2->GetTexture());
	sphere4->SetBumpMap(sphere3->GetBumpMap());
	SetTextureRepeating(sphere4->GetTexture(), true);
	SetTextureRepeating(sphere4->GetBumpMap(), true);
	SceneNode* sphereNode4 = new SceneNode(sphere4);
	sphereNode4->SetTransform(Matrix4::Scale(Vector3(5, 5, 5)) * Matrix4::Translation(Vector3(75, 50, 0)));
	sphereNode4->SetShader(reflectShader);
	sphereNode4->SetBoundingRadius(500.0f);
	sceneOne->AddChild(sphereNode4);

	Light* l = new Light(Vector3(16.0f, 200.0f, -491.0f), Vector4(1, 1, 1, 1), 4000.0f);
	l->setCastShadows(true);
	l->setShadowDir(l->GetPosition() + Vector3(0, 0, 1));
	lightsSceneOne.push_back(l);
}

void Renderer::setupSceneTwo() {
	HeightMap* terrain = new HeightMap(TEXTUREDIR"smile.raw");
	terrain->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"Barren Reds.jpg", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	terrain->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"Barren RedsDOT3.JPG", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));

	SetTextureRepeating(terrain->GetTexture(), true);
	SetTextureRepeating(terrain->GetBumpMap(), true);

	SceneNode* terrainNode = new SceneNode(terrain);

	terrainNode->SetTransform(Matrix4::Translation(Vector3(-1000, -20, -1000)));
	terrainNode->SetShader(sceneShader);
	terrainNode->SetBoundingRadius(5000.0f);

	sceneTwo->AddChild(terrainNode);

	hellData = new MD5FileData(MESHDIR"hellknight.md5mesh");

	hellNode = new MD5Node(*hellData);

	hellData->AddAnim(MESHDIR"idle2.md5anim");
	hellNode->PlayAnim(MESHDIR"idle2.md5anim");
	hellNode->SetBoundingRadius(500.0f);
	hellNode->SetTransform(Matrix4::Rotation(90, Vector3(0, -1, 0)));
	sceneTwo->AddChild(hellNode);

	Light* l = new Light(Vector3(0.0f, 210.0f, -500.0f), Vector4(1, 0, 0, 1), 4000.0f);
	l->setCastShadows(true);
	l->setShadowDir(l->GetPosition() + Vector3(0, 0, 1));

	Light* l2 = new Light(Vector3(-400.0f, 200.0f, -500.0f), Vector4(1, 1, 1, 1), 4000.0f);
	l2->setCastShadows(true);
	l2->setShadowDir(l2->GetPosition() + Vector3(0, 0, 1));

	Light* l3 = new Light(Vector3(400.0f, 200.0f, -500.0f), Vector4(1, 1, 1, 1), 4000.0f);
	l3->setCastShadows(true);
	l3->setShadowDir(l3->GetPosition() + Vector3(0, 0, 1));

	lightsSceneTwo.push_back(l2);
	lightsSceneTwo.push_back(l3);
	lightsSceneTwo.push_back(l);
}

void Renderer::setupSceneThree() {
	HeightMap* terrain = new HeightMap(TEXTUREDIR"smile.raw");
	terrain->SetTexture(SOIL_load_OGL_texture(TEXTUREDIR"Barren Reds.jpg", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));
	terrain->SetBumpMap(SOIL_load_OGL_texture(TEXTUREDIR"Barren RedsDOT3.JPG", SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, SOIL_FLAG_MIPMAPS));

	SetTextureRepeating(terrain->GetTexture(), true);
	SetTextureRepeating(terrain->GetBumpMap(), true);

	SceneNode* terrainNode = new SceneNode(terrain);

	terrainNode->SetTransform(Matrix4::Translation(Vector3(-1000, -20, -1000)));
	terrainNode->SetShader(sceneShader);
	terrainNode->SetBoundingRadius(5000.0f);

	sceneThree->AddChild(terrainNode);

	Vector4 colors[6] = {
		Vector4(1,0,0,1),
		Vector4(0,1,0,1),
		Vector4(0,0,1,1),
		Vector4(1,1,0,1),
		Vector4(1,0,1,1),
		Vector4(0,1,1,1)
	};
	OBJMesh* nanosuit = new OBJMesh(MESHDIR"nanosuit.obj");
	for (int i = 0; i < 5; ++i) {
		for (int j = 0; j < 5; ++j) {
			SceneNode* nanoNode = new SceneNode(nanosuit);
			nanoNode->SetShader(reflectShader);
			nanoNode->SetTransform(Matrix4::Scale(Vector3(10, 10, 10)) * Matrix4::Rotation(90, Vector3(0, 1, 0)) * Matrix4::Translation(Vector3(-90 - (j * 10), -2, 20 -(i * 10))));
			nanoNode->SetBoundingRadius(5000.0f);
			sceneThree->AddChild(nanoNode);

			Light* l = new Light(Vector3(1500.0f - (i * 500.0f), 210.0f, -00.0f + (j * 500.0f)), colors[(i + j) % 6], 1000.0f);
			l->setCastShadows(false);
			l->setShadowDir(Vector3(-235.0f, 28.0f, 1000.0f));
			lightsSceneThree.push_back(l);
		}
	}

	
}

void Renderer::UpdateScene(float msec) {
	if(moveCamOne)
		camOne->UpdateCamera(msec);
	else
		camTwo->UpdateCamera(msec);
	hellNode->Update(msec);
	fpsCounter += msec;
	frameCount++;
	if (fpsCounter > 1000) {
		fps = frameCount;
		frameCount = 0;
		fpsCounter = 0;
	}
	redIntensity += msec / 1000.0f;
	if (redIntensity >= 90.0f)
		redIntensity -= 90.0f;
	if(cycleScenes)
		sceneChangeTimer += msec;
	if (sceneChangeTimer >= 5000) {
		sceneChangeTimer -= 5000;
		switchScene();
	}

	lightMovement += msec / 1000.0f;
	if (lightMovement >= 180.0f)
		lightMovement -= 180.0f;
	for (int i = 0; i < lightsSceneThree.size(); ++i) {
		lightsSceneThree.at(i)->SetPosition(lightsSceneThree.at(i)->GetPosition() + Vector3(0, 0, sin(lightMovement) * 2));
	}
}

void Renderer::RenderScene() {
	currentCamera = camOne;
	modelMatrix.ToIdentity();
	if (drawSplitScreen)
		projMatrix = cameraSplitScreenPerspectiveMatrix;
	else
		projMatrix = cameraPerspectiveMatrix;
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	FillBuffers();
	if (useCubeShadows)
		DrawPointLightsCubeShadows();
	else
		DrawPointLights();
	CombineBuffers();
	if(applyRedPostProcess)
		DrawRedPostProcess();
	if(drawSplitScreen)
		glViewport(0, 0, width, height / 2);
	presentScene();
	glViewport(0, 0, width, height);

	if (drawSplitScreen) {
		currentCamera = camTwo;
		modelMatrix.ToIdentity();
		projMatrix = cameraSplitScreenPerspectiveMatrix;
		FillBuffers();
		if (useCubeShadows)
			DrawPointLightsCubeShadows();
		else
			DrawPointLights();
		CombineBuffers();
		if (applyRedPostProcess)
			DrawRedPostProcess();
		glViewport(0, height / 2, width, height /2);
		presentScene();
		glViewport(0, 0, width, height);
	}

	if(drawHUD)
		DrawHUD();

	SwapBuffers();
	glUseProgram(0);
	ClearNodeLists();
}

void Renderer::FillBuffers() {
	glBindFramebuffer(GL_FRAMEBUFFER, bufferFBO);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	
	viewMatrix = currentCamera->BuildViewMatrix();

	frameFrustum.FromMatrix(projMatrix*viewMatrix);
	BuildNodeLists(root);
	SortNodeLists();

	DrawNodes();

	ClearNodeLists();

	glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::DrawShadowMap(Light* l) {
	glBindFramebuffer(GL_FRAMEBUFFER, shadowFBO);

	glClear(GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, SHADOWSIZE, SHADOWSIZE);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

	SetCurrentShader(shadowCubeShader);

	//viewMatrix = Matrix4::BuildViewMatrix(l->GetPosition(), l->GetPosition() + Vector3(0, 0, 1));
	viewMatrix = Matrix4::BuildViewMatrix(l->GetPosition(), l->getShadowDir());
	projMatrix = shadowPerspectiveMatrix;
	shadowMatrix = biasMatrix * (projMatrix * viewMatrix);
	
	frameFrustum.FromMatrix(projMatrix*viewMatrix);
	BuildNodeLists(root);
	SortNodeLists();

	DrawNodes(false);

	ClearNodeLists();

	glUseProgram(0);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glViewport(0, 0, width, height);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	if(drawSplitScreen)
		projMatrix = cameraSplitScreenPerspectiveMatrix;
	else
		projMatrix = cameraPerspectiveMatrix;
}

void Renderer::DrawShadowCubeMap(Light* l) {
	glBindFramebuffer(GL_FRAMEBUFFER, shadowCubeMapFBO);

	glClear(GL_DEPTH_BUFFER_BIT);
	glViewport(0, 0, SHADOWSIZE, SHADOWSIZE);
	glColorMask(GL_FALSE, GL_FALSE, GL_FALSE, GL_FALSE);

	for (int i = 0; i < 6; ++i) {
		glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, shadowCubeMapTex, 0);
		glClear(GL_DEPTH_BUFFER_BIT);
	}

	SetCurrentShader(shadowShader);
	projMatrix = shadowPerspectiveMatrix;

	for (int i = 0; i < 6; ++i) {
		glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, shadowCubeMapTex, 0);
		viewMatrix = Matrix4::BuildViewMatrix(l->GetPosition(), l->GetPosition() + cubeMapPos[i]);
		shadowMatrix = biasMatrix * (projMatrix * viewMatrix);

		frameFrustum.FromMatrix(projMatrix*viewMatrix);
		BuildNodeLists(root);
		SortNodeLists();

		DrawNodes(false);

		ClearNodeLists();
	}

	glUseProgram(0);
	glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
	glViewport(0, 0, width, height);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	if (drawSplitScreen)
		projMatrix = cameraSplitScreenPerspectiveMatrix;
	else
		projMatrix = cameraPerspectiveMatrix;
}

void Renderer::DrawPointLightsCubeShadows() {
	vector<Light*> shadowLights;
	vector<Light*> nonShadowLights;

	for (vector<Light*>::const_iterator i = lights->begin(); i != lights->end(); ++i) {
		Light* l = (*i);
		if (l->castShadows()) {
			shadowLights.push_back(l);
		}
		else {
			nonShadowLights.push_back(l);
		}
	}

	glBindFramebuffer(GL_FRAMEBUFFER, pointLightFBO);
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	for (vector<Light*>::const_iterator i = shadowLights.begin(); i != shadowLights.end(); ++i) {
		Light* l = (*i);
		//###Shadow###
		DrawShadowCubeMap(l);
		//###Light###
		SetCurrentShader(pointLightShaderCubeShadows);

		glBindFramebuffer(GL_FRAMEBUFFER, pointLightFBO);

		glClearColor(0, 0, 0, 1);

		glBlendFunc(GL_ONE, GL_ONE);

		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "depthTex"), 3);
		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "normTex"), 4);
		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "posTex"), 5);
		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "shadowTex"), 6);

		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, bufferDepthTex);

		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D, bufferNormalTex);

		glActiveTexture(GL_TEXTURE5);
		glBindTexture(GL_TEXTURE_2D, bufferPosTex);

		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_CUBE_MAP, shadowCubeMapTex);

		viewMatrix = currentCamera->BuildViewMatrix();

		glUniform3fv(glGetUniformLocation(currentShader->GetProgram(), "cameraPos"), 1, (float*)&currentCamera->GetPosition());
		glUniform2f(glGetUniformLocation(currentShader->GetProgram(), "pixelSize"), 1.0f / width, 1.0f / height);

		Vector3 translate = Vector3((RAW_HEIGHT*HEIGHTMAP_X / 2.0f), 500, (RAW_HEIGHT*HEIGHTMAP_Z / 2.0f));

		float radius = l->GetRadius();

		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "castShadows"), l->castShadows());
		glUniform1f(glGetUniformLocation(currentShader->GetProgram(), "far_plane"), 10000.0f);
		glUniform1f(glGetUniformLocation(currentShader->GetProgram(), "near_plane"), 1.0f);
		glUniform3fv(glGetUniformLocation(currentShader->GetProgram(), "lightPos"), 1, (float*)&l->GetPosition());

		modelMatrix =
			Matrix4::Rotation(1, Vector3(0, 0, 0)) *
			Matrix4::Translation(l->GetPosition()) *
			Matrix4::Scale(Vector3(radius, radius, radius));

		SetShaderLight(*l);
		UpdateShaderMatrices();

		float dist = (l->GetPosition() - currentCamera->GetPosition()).Length();
		if (dist < radius) {
			glCullFace(GL_FRONT);
		}
		else {
			glCullFace(GL_BACK);
		}
		lightSphere->Draw();


		glCullFace(GL_BACK);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glClearColor(0.2f, 0.2f, 0.2f, 1);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glUseProgram(0);
	}

	if (nonShadowLights.size() != 0) {
		SetCurrentShader(pointLightShader);

		glBindFramebuffer(GL_FRAMEBUFFER, pointLightFBO);

		glClearColor(0, 0, 0, 1);

		glBlendFunc(GL_ONE, GL_ONE);

		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "depthTex"), 3);
		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "normTex"), 4);
		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "posTex"), 5);
		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "shadowTex"), 6);

		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, bufferDepthTex);

		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D, bufferNormalTex);

		glActiveTexture(GL_TEXTURE5);
		glBindTexture(GL_TEXTURE_2D, bufferPosTex);

		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D, shadowTex);

		glUniform3fv(glGetUniformLocation(currentShader->GetProgram(), "cameraPos"), 1, (float*)&currentCamera->GetPosition());
		glUniform2f(glGetUniformLocation(currentShader->GetProgram(), "pixelSize"), 1.0f / width, 1.0f / height);

		Vector3 translate = Vector3((RAW_HEIGHT*HEIGHTMAP_X / 2.0f), 500, (RAW_HEIGHT*HEIGHTMAP_Z / 2.0f));

		for (vector<Light*>::const_iterator i = nonShadowLights.begin(); i != nonShadowLights.end(); ++i) {
			Light* l = (*i);
			float radius = l->GetRadius();

			glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "castShadows"), l->castShadows());

			modelMatrix =
				Matrix4::Translation(l->GetPosition()) *
				Matrix4::Scale(Vector3(radius, radius, radius));

			SetShaderLight(*l);
			UpdateShaderMatrices();

			float dist = (l->GetPosition() - currentCamera->GetPosition()).Length();
			if (dist < radius) {
				glCullFace(GL_FRONT);
			}
			else {
				glCullFace(GL_BACK);
			}

			lightSphere->Draw();

		}

		glCullFace(GL_BACK);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glClearColor(0.2f, 0.2f, 0.2f, 1);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glUseProgram(0);
	}
}

void Renderer::DrawPointLights() {
	vector<Light*> shadowLights;
	vector<Light*> nonShadowLights;

	for (vector<Light*>::const_iterator i = lights->begin(); i != lights->end(); ++i) {
		Light* l = (*i);
		if (l->castShadows()) {
			shadowLights.push_back(l);
		}
		else {
			nonShadowLights.push_back(l);
		}
	}

	glBindFramebuffer(GL_FRAMEBUFFER, pointLightFBO);
	glClearColor(0, 0, 0, 1);
	glClear(GL_COLOR_BUFFER_BIT);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	for (vector<Light*>::const_iterator i = shadowLights.begin(); i != shadowLights.end(); ++i) {
		Light* l = (*i);

		DrawShadowMap(l);

		SetCurrentShader(pointLightShader);

		glBindFramebuffer(GL_FRAMEBUFFER, pointLightFBO);

		glClearColor(0, 0, 0, 1);

		glBlendFunc(GL_ONE, GL_ONE);

		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "depthTex"), 3);
		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "normTex"), 4);
		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "posTex"), 5);
		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "shadowTex"), 6);

		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, bufferDepthTex);

		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D, bufferNormalTex);

		glActiveTexture(GL_TEXTURE5);
		glBindTexture(GL_TEXTURE_2D, bufferPosTex);

		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D, shadowTex);

		viewMatrix = currentCamera->BuildViewMatrix();

		glUniform3fv(glGetUniformLocation(currentShader->GetProgram(), "cameraPos"), 1, (float*)&currentCamera->GetPosition());
		glUniform2f(glGetUniformLocation(currentShader->GetProgram(), "pixelSize"), 1.0f / width, 1.0f / height);

		Vector3 translate = Vector3((RAW_HEIGHT*HEIGHTMAP_X / 2.0f), 500, (RAW_HEIGHT*HEIGHTMAP_Z / 2.0f));

		float radius = l->GetRadius();

		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "castShadows"), l->castShadows());

		modelMatrix =
			Matrix4::Rotation(1, Vector3(0, 0, 0)) *
			Matrix4::Translation(l->GetPosition()) *
			Matrix4::Scale(Vector3(radius, radius, radius));

		SetShaderLight(*l);
		UpdateShaderMatrices();

		float dist = (l->GetPosition() - currentCamera->GetPosition()).Length();
		if (dist < radius) {
			glCullFace(GL_FRONT);
		}
		else {
			glCullFace(GL_BACK);
		}
		lightSphere->Draw();


		glCullFace(GL_BACK);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glClearColor(0.2f, 0.2f, 0.2f, 1);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glUseProgram(0);
	}

	if (nonShadowLights.size() != 0) {
		SetCurrentShader(pointLightShader);

		glBindFramebuffer(GL_FRAMEBUFFER, pointLightFBO);

		glClearColor(0, 0, 0, 1);

		glBlendFunc(GL_ONE, GL_ONE);

		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "depthTex"), 3);
		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "normTex"), 4);
		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "posTex"), 5);
		glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "shadowTex"), 6);

		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_2D, bufferDepthTex);

		glActiveTexture(GL_TEXTURE4);
		glBindTexture(GL_TEXTURE_2D, bufferNormalTex);

		glActiveTexture(GL_TEXTURE5);
		glBindTexture(GL_TEXTURE_2D, bufferPosTex);

		glActiveTexture(GL_TEXTURE6);
		glBindTexture(GL_TEXTURE_2D, shadowTex);

		glUniform3fv(glGetUniformLocation(currentShader->GetProgram(), "cameraPos"), 1, (float*)&currentCamera->GetPosition());
		glUniform2f(glGetUniformLocation(currentShader->GetProgram(), "pixelSize"), 1.0f / width, 1.0f / height);

		Vector3 translate = Vector3((RAW_HEIGHT*HEIGHTMAP_X / 2.0f), 500, (RAW_HEIGHT*HEIGHTMAP_Z / 2.0f));

		for (vector<Light*>::const_iterator i = nonShadowLights.begin(); i != nonShadowLights.end(); ++i) {
			Light* l = (*i);
			float radius = l->GetRadius();

			glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "castShadows"), l->castShadows());

			modelMatrix =
				Matrix4::Translation(l->GetPosition()) *
				Matrix4::Scale(Vector3(radius, radius, radius));

			SetShaderLight(*l);
			UpdateShaderMatrices();

			float dist = (l->GetPosition() - currentCamera->GetPosition()).Length();
			if (dist < radius) {
				glCullFace(GL_FRONT);
			}
			else {
				glCullFace(GL_BACK);
			}

			lightSphere->Draw();

		}

		glCullFace(GL_BACK);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glClearColor(0.2f, 0.2f, 0.2f, 1);

		glBindFramebuffer(GL_FRAMEBUFFER, 0);
		glUseProgram(0);
	}
}

void Renderer::CombineBuffers() {

	glBindFramebuffer(GL_FRAMEBUFFER, combinedFBO);
	SetCurrentShader(combineShader);

	if (drawSplitScreen)
		projMatrix = cameraSplitScreenPerspectiveMatrix;
	else
		projMatrix = cameraPerspectiveMatrix;
	glUniformMatrix4fv(glGetUniformLocation(currentShader->GetProgram(), "oriProjMatrix"), 1, false, (float*)&projMatrix);

	projMatrix = Matrix4::Orthographic(-1, 1, 1, -1, -1, 1);
	viewMatrix = currentCamera->BuildViewMatrix();
	UpdateShaderMatrices();

	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 2);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "emissiveTex"), 3);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "specularTex"), 4);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "posTex"), 5);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "depthTex"), 6);
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "skybox"), 7);

	
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, bufferColourTex);

	glActiveTexture(GL_TEXTURE3);
	glBindTexture(GL_TEXTURE_2D, lightEmissiveTex);

	glActiveTexture(GL_TEXTURE4);
	glBindTexture(GL_TEXTURE_2D, lightSpecularTex);

	glActiveTexture(GL_TEXTURE5);
	glBindTexture(GL_TEXTURE_2D, bufferPosTex);

	glActiveTexture(GL_TEXTURE6);
	glBindTexture(GL_TEXTURE_2D, bufferDepthTex);

	glActiveTexture(GL_TEXTURE7);
	glBindTexture(GL_TEXTURE_CUBE_MAP, skybox);

	fullScreenQuad->Draw();

	glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::DrawRedPostProcess() {
	glBindFramebuffer(GL_FRAMEBUFFER, combinedFBO);
	SetCurrentShader(redShader);

	UpdateShaderMatrices();

	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 2);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, combinedTex);

	glUniform1f(glGetUniformLocation(currentShader->GetProgram(), "intensity"), sin(redIntensity));

	fullScreenQuad->Draw();

	glUseProgram(0);
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void Renderer::presentScene() {
	SetCurrentShader(presentShader);

	UpdateShaderMatrices();

	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 2);
	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, combinedTex);

	fullScreenQuad->Draw();

	glUseProgram(0);
}

void Renderer::DrawHUD() {
	SetCurrentShader(textShader);
	glUseProgram(currentShader->GetProgram());
	glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE);
	glDisable(GL_DEPTH_TEST);

	DrawText(to_string((int)fps), Vector3(0, 0, 0), 16.0f);
	DrawText("Controls:", Vector3(0, 20, 0), 16.0f);
	DrawText("  Toggle draw HUD: P", Vector3(0, 40, 0), 16.0f);
	DrawText("  Camera: W,S,A,D,Shift,Space", Vector3(0, 60, 0), 16.0f);
	DrawText("  Toggle splitscreen: U", Vector3(0, 80, 0), 16.0f);
	DrawText("  Toggle post process: I", Vector3(0, 100, 0), 16.0f);
	DrawText("  Toggle camera to move: J", Vector3(0, 120, 0), 16.0f);
	DrawText("  Cycle scenes: Left, Right", Vector3(0, 140, 0), 16.0f);
	DrawText("  Toggle auto scene cycle: Pause", Vector3(0, 160, 0), 16.0f);
	DrawText("  Toggle cube mapped shadows: O", Vector3(0, 180, 0), 16.0f);
	DrawText("  Toggle draw shadows: L", Vector3(0, 200, 0), 16.0f);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_DEPTH_TEST);

	glUseProgram(0);
}

void Renderer::DrawText(const std::string& text, const Vector3& position, const float size, const bool perspective) {
	TextMesh* mesh = new TextMesh(text, *basicFont);

	if (perspective) {
		modelMatrix = Matrix4::Translation(position) * Matrix4::Scale(Vector3(size, size, 1));
		viewMatrix = currentCamera->BuildViewMatrix();
		projMatrix = Matrix4::Perspective(1.0f, 20000.0f, (float)width / (float)height, 45.0f);
	}
	else {
		modelMatrix = Matrix4::Translation(Vector3(position.x, height - position.y, position.z))*Matrix4::Scale(Vector3(size, size, 1));
		viewMatrix.ToIdentity();
		projMatrix = Matrix4::Orthographic(-1.0f, 1.0f, (float)width, 0.0f, (float)height, 0.0f);
	}

	UpdateShaderMatrices();
	mesh->Draw();

	delete mesh;
}

void Renderer::BuildNodeLists(SceneNode* from) {
	if (frameFrustum.InsideFrustum(*from)) {
		Vector3 dir = from->GetWorldTransform().GetPositionVector() - currentCamera->GetPosition();
		from->SetCameraDistance(Vector3::Dot(dir, dir));

		if (from->GetColour().w < 1.0f) {
			transparentNodeList.push_back(from);
		}
		else {
			nodeList.push_back(from);
		}
	}

	for (vector<SceneNode*>::const_iterator i = from->GetChildIteratorStart(); i != from->GetChildIteratorEnd(); ++i) {
		BuildNodeLists((*i));
	}
}

void Renderer::SortNodeLists() {
	std::sort(transparentNodeList.begin(), transparentNodeList.end(), SceneNode::CompareByCameraDistance);
	std::sort(nodeList.begin(), nodeList.end(), SceneNode::CompareByCameraDistance);
}

void Renderer::DrawNodes(bool useNodeShader) {
	for (vector<SceneNode*>::const_iterator i = nodeList.begin(); i != nodeList.end(); ++i) {
		DrawNode((*i), useNodeShader);
	}

	for (vector<SceneNode*>::const_reverse_iterator i = transparentNodeList.rbegin(); i != transparentNodeList.rend(); ++i) {
		DrawNode((*i), useNodeShader);
	}
}

void Renderer::DrawNode(SceneNode* n, bool useNodeShader) {
	if (n->GetMesh()) {
		if (useNodeShader) {
			if (n->GetShader()) {
				SetCurrentShader(n->GetShader());
				if (n->GetTexture()) {
					glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);
					glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "useDiffuse"), true);
				}
				else
					glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "useDiffuse"), false);
				if (n->GetBumpMap()) {
					glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "bumpTex"), 1);
					glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "useBumpMap"), true);
				}
				else
					glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "useBumpMap"), false);

				if (currentShader == reflectShader) {
					glUniform3fv(glGetUniformLocation(currentShader->GetProgram(), "cameraPos"), 1, (float*)&currentCamera->GetPosition());

					glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "cubeTex"), 2);
					glActiveTexture(GL_TEXTURE2);
					glBindTexture(GL_TEXTURE_CUBE_MAP, skybox);
				}
			}
			else {
				SetCurrentShader(sceneShader);
				glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 0);
				glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "bumpTex"), 1);
			}
		}

		modelMatrix = n->GetTransform();
		UpdateShaderMatrices();
		n->Draw(*this);
	}
}

void Renderer::ClearNodeLists() {
	transparentNodeList.clear();
	nodeList.clear();
}