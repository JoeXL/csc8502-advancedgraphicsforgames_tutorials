#version 150 core

uniform sampler2D diffuseTex;
uniform float intensity;

in Vertex {
	vec2 texCoord;
} IN;

out vec4 fragColour;

void main(void) {
	vec3 diffuse = texture(diffuseTex, IN.texCoord).xyz;
	
	fragColour = vec4(diffuse, 1.0) * vec4(1.0, intensity * 0.5 + 0.5, intensity * 0.5 + 0.5, 1.0);
}