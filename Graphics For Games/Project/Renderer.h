#pragma once
#include "pch.h"

#include "../../nclgl/OGLRenderer.h"
#include "../../nclgl/Camera.h"
#include "../../nclgl/OBJMesh.h"
#include "../../nclgl/MD5Mesh.h"
#include "../../nclgl/MD5Node.h"
#include "../../nclgl/HeightMap.h"
#include "../../nclgl/Frustum.h"
#include "../../nclgl/Light.h"
#include "TextMesh.h"

#include <algorithm>

#define SHADOWSIZE 2048

class Renderer : public OGLRenderer {
public:
	Renderer(Window& parent);
	virtual ~Renderer(void);

	virtual void UpdateScene(float msec);
	virtual void RenderScene();

	void toggleDrawHUD() {
		drawHUD = !drawHUD;
	}

	void toggleRedPostProcess() {
		applyRedPostProcess = !applyRedPostProcess;
	}

	void toggleDrawSplitScreen() {
		drawSplitScreen = !drawSplitScreen;
	}

	void toggleCycleScenes() {
		cycleScenes = !cycleScenes;
	}

	void toggleMoveCamOne() {
		moveCamOne = !moveCamOne;
	}

	void toggleUseCubeShadows() {
		useCubeShadows = !useCubeShadows;
	}

	void toggleCastShadows() {
		for (int i = 0; i < lights->size(); ++i) {
			lights->at(i)->setCastShadows(!lights->at(i)->castShadows());
		}
	}

	void switchScene(bool forward = true) {
		if (forward)
			currentScene++;
		else
			currentScene--;
		if (currentScene >= 3)
			currentScene = 0;
		else if (currentScene < 0)
			currentScene = 2;
		switch (currentScene) {
		case 0: {
			root = sceneOne;
			lights = &lightsSceneOne;
			camOne->SetPosition(Vector3(16.0f, 350.0f, -805.0f));
			camOne->SetYaw(180.0f);
			camOne->SetPitch(-10.0f);
			break;
		}
		case 1: {
			root = sceneTwo;
			lights = &lightsSceneTwo;
			camOne->SetPosition(Vector3(-160.0f, 190.0f, -285.0f));
			camOne->SetYaw(200.0f);
			camOne->SetPitch(-20.0f);
			break;
		}
		case 2: {
			root = sceneThree;
			lights = &lightsSceneThree;
			camOne->SetPosition(Vector3(2710.0f, 2230.0f, 1115.0f));
			camOne->SetYaw(90.0f);
			camOne->SetPitch(-55.0f);
			break;
		}
		}
	}

protected:
	void setupSceneOne();
	void setupSceneTwo();
	void setupSceneThree();

	void generateGBuffer();
	void generateLightBuffer();
	void generateShadowBuffer();
	void generateShadowCubeMapBuffer();
	void generateCombinedBuffer();

	void FillBuffers();
	void DrawPointLights();
	void DrawPointLightsCubeShadows();
	void CombineBuffers();
	void DrawRedPostProcess();
	void DrawHUD();
	void DrawShadowMap(Light* light);
	void DrawShadowCubeMap(Light* light);
	void presentScene();

	void GenerateScreenTexture(GLuint& into, bool depth = false);
	void GenerateShadowTexture(GLuint& into);
	void GenerateShadowCubeMapTexture(GLuint& into);

	void BuildNodeLists(SceneNode* from);
	void SortNodeLists();
	void ClearNodeLists();
	void DrawNodes(bool useNodeShader = true);
	void DrawNode(SceneNode* n, bool useNodeShader);

	Matrix4 getLightViewMatrix() { return Matrix4::BuildViewMatrix(lights->at(0)->GetPosition(), lights->at(0)->GetPosition() + Vector3(0, 0, 1)); }

	void	DrawText(const std::string &text, const Vector3 &position, const float size = 10.0f, const bool perspective = false);

	bool drawHUD = true;
	bool applyRedPostProcess = false;
	float fpsCounter;
	int frameCount;
	int fps;
	float redIntensity = 0;
	float lightMovement = 0;
	bool cycleScenes = false;
	int sceneChangeTimer = 0;
	bool moveCamOne = true;
	bool useCubeShadows = false;

	bool drawSplitScreen = false;
	int currentScreenNum = 0;

	SceneNode* root;
	vector<Light*>* lights;

	SceneNode* sceneOne;
	SceneNode* sceneTwo;
	SceneNode* sceneThree;

	vector<Light*> lightsSceneOne;
	vector<Light*> lightsSceneTwo;
	vector<Light*> lightsSceneThree;

	int currentScene = 0;
	
	OBJMesh* lightSphere;
	Mesh* fullScreenQuad;
	Camera* currentCamera;
	Camera* camOne;
	Camera* camTwo;

	Matrix4 cameraPerspectiveMatrix;
	Matrix4 cameraSplitScreenPerspectiveMatrix;
	Matrix4 shadowPerspectiveMatrix;

	MD5FileData* hellData;
	MD5Node* hellNode;

	Font* basicFont;

	Shader* sceneShader;
	Shader* pointLightShader;
	Shader* pointLightShaderCubeShadows;
	Shader* combineShader;
	Shader* shadowShader;
	Shader* shadowCubeShader;
	Shader* textShader;
	Shader* reflectShader;
	Shader* presentShader;
	Shader* redShader;

	Frustum frameFrustum;

	vector<SceneNode*> transparentNodeList;
	vector<SceneNode*> nodeList;

	GLuint skybox;

	GLenum gBuffer[3];
	GLenum lightBuffer[2];
	GLenum combinedBuffer[1];

	GLuint bufferFBO;
	GLuint bufferColourTex;
	GLuint bufferNormalTex;
	GLuint bufferPosTex;
	GLuint bufferDepthTex;

	GLuint pointLightFBO;
	GLuint lightEmissiveTex;
	GLuint lightSpecularTex;

	GLuint shadowFBO;
	GLuint shadowTex;

	GLuint shadowCubeMapFBO;
	GLuint shadowCubeMapTex;

	GLuint combinedFBO;
	GLuint combinedTex;

	Vector3 cubeMapPos[6] = {
		Vector3(-1,0,0),
		Vector3(1,0,0),
		Vector3(0,-1,0.01f),
		Vector3(0,1,0.01f),
		Vector3(0,0,1),
		Vector3(0,0,-1)
	};
};

