#include "Renderer.h"



Renderer::Renderer(Window& parent) : OGLRenderer(parent) {
	CubeRobot::CreateCube();
	camera = new Camera();
	camera->SetSpeed(0.01);

	currentShader = new Shader(SHADERDIR"SceneVertex.glsl", SHADERDIR"SceneFragment.glsl");

	if (!currentShader->LinkProgram()) {
		return;
	}

	projMatrix = Matrix4::Perspective(1.0f, 10000.0f, (float)width / (float)height, 45.0f);

	camera->SetPosition(Vector3(0, 30, 175));

	root = new SceneNode();
	root->AddChild(new CubeRobot());
	root->SetShader(currentShader);

	CubeRobot* tmp = new CubeRobot;
	tmp->SetTransform(tmp->GetTransform() * Matrix4::Translation(Vector3(80, 0, 0)));
	root->AddChild(tmp);

	for (vector<SceneNode*>::const_iterator i = tmp->GetChildIteratorStart(); i != tmp->GetChildIteratorEnd(); ++i) {
		(*i)->SetShader(currentShader);
	}

	//root->SetTransform(root->GetTransform() * Matrix4::Scale(Vector3(10, 10, 10)));

	glEnable(GL_DEPTH_TEST);
	init = true;
}


Renderer::~Renderer(void) {
	delete root;
	CubeRobot::DeleteCube();
}

void Renderer::UpdateScene(float msec) {
	camera->UpdateCamera(msec);
	viewMatrix = camera->BuildViewMatrix();
	root->Update(msec);
}

void Renderer::RenderScene() {
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	//glUseProgram(currentShader->GetProgram());
	//UpdateShaderMatrices();

	//glUniform1i(glGetUniformLocation(currentShader->GetProgram(), "diffuseTex"), 1);

	DrawNode(root);

	glUseProgram(0);
	SwapBuffers();
}

void Renderer::DrawNode(SceneNode* n) {
	if (n->GetMesh() && n->GetShader()) {
		glUseProgram(n->GetShader()->GetProgram());
		UpdateShaderMatrices();

		glUniform1i(glGetUniformLocation(n->GetShader()->GetProgram(), "diffuseTex"), 1);

		Matrix4 transform = n->GetWorldTransform() * Matrix4::Scale(n->GetModelScale());

		glUniformMatrix4fv(glGetUniformLocation(n->GetShader()->GetProgram(), "modelMatrix"), 1, false, (float*)&transform);
		glUniform4fv(glGetUniformLocation(n->GetShader()->GetProgram(), "nodeColour"), 1, (float*)&n->GetColour());
		glUniform1i(glGetUniformLocation(n->GetShader()->GetProgram(), "useTexture"), (int)n->GetMesh()->GetTexture());
		n->Draw(*this);

		//glUseProgram(0);

	}

	for (vector<SceneNode*>::const_iterator i = n->GetChildIteratorStart(); i != n->GetChildIteratorEnd(); ++i) {
		DrawNode(*i);
	}
}