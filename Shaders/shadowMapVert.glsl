#version 150

uniform mat4 modelMatrix;
uniform mat4 viewMatrix;
uniform mat4 projMatrix;
uniform mat4 shadowMVP;

in vec3 position;
in vec3 colour;
in vec3 normal;
in vec3 tangent;
in vec2 texCoord;

out Vertex {
	vec4 shadowProj;
} OUT;

void main(void) {
	OUT.shadowProj = (shadowMVP * vec4(position + (normal * 1.5), 1));
	gl_Position = (projMatrix * viewMatrix * modelMatrix) * vec4(position, 1.0);
}