#pragma once
#include "OGLRenderer.h"

class TextureLoader
{
public:
	TextureLoader(TextureLoader const&) = delete;
	void operator=(TextureLoader const&) = delete;

	static GLuint loadTexture(string filename) {
		string path(TEXTUREDIR);
		GLuint i = SOIL_load_OGL_texture((path + filename).c_str(), SOIL_LOAD_AUTO, SOIL_CREATE_NEW_ID, 0);
		if (!i)
			cout << "Failed to load texture: " << filename << endl;
		return i;
	}

private:
	TextureLoader() {};
	~TextureLoader() {};
};

