#version 150 core

uniform sampler2D diffuseTex;
uniform sampler2D emissiveTex;
uniform sampler2D specularTex;
uniform sampler2D shadowMapTex;
uniform sampler2D posTex;
uniform sampler2D depthTex;
uniform samplerCube skybox;

in Vertex {
	vec2 texCoord;
	vec3 normal;
} IN;

out vec4 fragColour;

void main(void) {
	vec3 diffuse = texture(diffuseTex, IN.texCoord).xyz;
	vec3 light = texture(emissiveTex, IN.texCoord).xyz;
	vec3 specular = texture(specularTex, IN.texCoord).xyz;
	vec3 pos = texture(posTex, IN.texCoord).xyz;
	float depth = texture(depthTex, IN.texCoord).r;
	
	if(depth == 1.0) {
		vec4 sky = texture(skybox, normalize(IN.normal));
		fragColour = sky;
	}
	else {
		fragColour.xyz = diffuse * 0.2;
		fragColour.xyz += diffuse * light;
		fragColour.xyz += specular;
		fragColour.a = 1.0;
	}
	
	//fragColour.xyz = light;
	//linearDepth = (2 * 1) / (10000 + 1 - linearDepth * (10000 - 1));
	//fragColour.xyz = vec3(linearDepth, linearDepth, linearDepth);
}