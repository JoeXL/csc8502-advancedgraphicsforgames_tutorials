#version 150 core

uniform mat4 oriProjMatrix;
uniform mat4 projMatrix;
uniform mat4 viewMatrix;

in vec3 position;
in vec2 texCoord;

out Vertex {
	vec2 texCoord;
	vec3 normal;
} OUT;

void main(void) {
	gl_Position = projMatrix * vec4(position, 1.0);
	OUT.texCoord = texCoord;
	
	vec3 multiply = vec3(0,0,0);
	multiply.x = 1.0f / oriProjMatrix[0][0];
	multiply.y = -1.0f / oriProjMatrix[1][1];
	
	vec3 tempPos = (position * multiply) - vec3(0, 0, 1);
	OUT.normal = transpose(mat3(viewMatrix)) * normalize(tempPos);
}