#version 150 core

uniform sampler2D diffuseTex;
uniform sampler2D bumpTex;
uniform samplerCube cubeTex;

uniform vec3 cameraPos;
uniform bool useDiffuse;
uniform bool useBumpMap;

in Vertex {
	vec4 colour;
	vec2 texCoord;
	vec3 normal;
	vec3 tangent;
	vec3 binormal;
	vec3 worldPos;
	vec3 fragPos;
} IN;

out vec4 fragColour[3];

void main(void) {
	vec3 normal;
	if(useBumpMap) {
		mat3 TBN = mat3(IN.tangent, IN.binormal, IN.normal);
		normal = normalize(TBN * normalize((texture2D(bumpTex, IN.texCoord).rgb) * 2.0 - 1.0));
	}
	else {
		normal = normalize(IN.normal);
	}
	vec3 incident = normalize(IN.worldPos - cameraPos);
	vec4 reflection = texture(cubeTex, reflect(incident, normal));
	
	vec4 diffuse = vec4(1.0,0.0,0.0,1.0);
	if(useDiffuse) {
		diffuse = 0.5 * texture2D(diffuseTex, IN.texCoord) + 0.5 * reflection;
	}
	else {
		diffuse = reflection;
	}
	fragColour[0] = diffuse;
	fragColour[1] = vec4(normal.xyz * 0.5 + 0.5, 1.0);
	fragColour[2] = vec4(IN.worldPos.xyz * 0.5 + 0.5, 1.0);
}